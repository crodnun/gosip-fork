// Copyright 2020 Justine Alexandra Roberts Tunney
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"io/ioutil"
	"encoding/json"

	"bitbucket.org/crodnun/gosip-fork/call"
)

// Program options, for now just pass the local IP to be used if we want to force its value (if not it is auto-calculated)
var (
	addressFlag  = flag.String("address", "", "Public IP (or hostname) of the local machine. Defaults to asking an untrusted webserver.")
	jsonFile = flag.String("conf", "call.json", "Path for the JSON configuration file we want to use to orchestrate the call")
)

// This type models the call script/JSON
type CallInstructionsSet struct {
	CallInstructionsSet []CallInstruction `json:"call"` 
}

type CallInstruction struct {
	Action int `json:"action"`
	Parameters []string `json:"parameters"`
}

// SetupCloseHandler creates a 'listener' on a new goroutine which will notify the
// program if it receives an interrupt from the OS. We then handle this by calling
// our clean up procedure and exiting the program.
func SetupCloseHandler() {
	c := make(chan os.Signal)
        signal.Notify(c, os.Interrupt, syscall.SIGTERM)
        go func() {
	   <-c
	   fmt.Println("\r- Ctrl+C pressed in Terminal")
	   os.Exit(0)
        }()
}

func runCall(options call.CallOptions, logicCh chan call.CallEvent) {
	call.NewCall(options, logicCh)
}

func main() {
	//  Support for Ctrl +c 
	SetupCloseHandler()

	// log flags
	log.SetFlags(log.LstdFlags | log.Lmicroseconds | log.Lshortfile)

	// Parse call options - add more in the future
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s URI\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
	if len(flag.Args()) != 1 {
		flag.Usage()
		os.Exit(1)
	}

	// Read call instructions from JSON file and unmarshall them into call instructions
	file, error1 := ioutil.ReadFile(*jsonFile)
	if error1 != nil {
	        os.Exit(1);
	}

	// Show JSON configuration read
        fmt.Println(string(file))

	callInstructions := CallInstructionsSet{}
	err := json.Unmarshal([]byte(file), &callInstructions)

	if err != nil {
		fmt.Println("ERROR: Unable to parse call configuration from input JSON (", err, ")")
		os.Exit(1);
	}

	// Show call instructions read from input JSON
	for i := 0; i < len(callInstructions.CallInstructionsSet); i++ {
		fmt.Println("Action: ", callInstructions.CallInstructionsSet[i].Action)
		for j := 0; j < len(callInstructions.CallInstructionsSet[i].Parameters); j++  {
			fmt.Println("Parameter: ", callInstructions.CallInstructionsSet[i].Parameters[j])
		}
	}

	// Initialize call options
	options := call.CallOptions{ flag.Args()[0], *addressFlag }

	// Create the comms channel
	ch := make(chan call.CallEvent)

	fmt.Println("Waiting for call events ....")

	// Run the call in background and wait for events
	go runCall(options, ch)

	// Wait for call events to take the suitable decisions based on configured JSON actions
	for ;;  {
	select {
		case event := <-ch:
			fmt.Println("Received call event ", event.EventType)
			switch event.EventType {
			case call.CALL_EVENT_ANSWER:
				fmt.Println("Call was answered!")
				// Take here the suitable decisions - send an event towards the call to modify its behaviour
				// at dothis text we can pass JSON information to modify the call 
				logicEvent := call.CallEvent{ call.CALL_EVENT_NOTIFY, "{ do this }" }
				// Send the logic event to call - to modify its management
				ch <- logicEvent
			}
	}
	}

}
