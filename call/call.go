// Copyright 2020 Justine Alexandra Roberts Tunney
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package call

// #cgo pkg-config: ncurses libpulse-simple
// #include <stdlib.h>
// #include <ncurses.h>
// #include <pulse/simple.h>
// #include <pulse/error.h>
import "C"

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/jart/gosip/dialog"
	"github.com/jart/gosip/dsp"
	"github.com/jart/gosip/rtp"
	"github.com/jart/gosip/sdp"
	"github.com/jart/gosip/sip"
	"github.com/jart/gosip/util"
)

// Call options
type CallOptions struct {
	RequestURIString string   // Remote peer URI
	PublicIP string           // local IP address

	// Add here future options like scheduling and sequences to be executed
}

// Call events that logic and call can send using the comms channel created
// They are identified by a CallEventId
type CallEventType int

const (
	CALL_EVENT_ANSWER CallEventType = 0   // Call was answered
	CALL_EVENT_BYE CallEventType = 1      // Call finished
	CALL_EVENT_NOTIFY CallEventType = 2   // Logic notification
)

// Define a generic struct to deal with events
// Every event will contain the following information:
// - The eventType (see enum above)
// - The eventData (JSON string that we can unmarshall into the expected struct based on eventType)
type CallEvent struct {
    EventType CallEventType 
    EventData string
}

const (
	hz     = 8000
	chans  = 1
	ptime  = 20
	ssize  = 2
	psamps = hz / (1000 / ptime) * chans
	pbytes = psamps * ssize
)

func NewCall(options CallOptions, logicCh chan CallEvent) {
	// Called party
	requestURI, err := sip.ParseURI([]byte(options.RequestURIString))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Bad Request URI: %s\n", err.Error())
		os.Exit(1)
	}

	// Get Public local IP Address if not supplied
	publicIP := options.PublicIP
	if publicIP == "" {
		publicIP, err = getPublicIP()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to get local IP: %s\n", err.Error())
			os.Exit(1)
		}
	}

	// Create RTP Session
	rs, err := rtp.NewSession("")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create RTP session: %s\n", err.Error())
		os.Exit(1)
	}
	defer rs.Close()
	rtpPort := uint16(rs.Sock.LocalAddr().(*net.UDPAddr).Port)

	// Construct SIP INVITE
	invite := &sip.Msg{
		Method:  sip.MethodInvite,
		Request: requestURI,
		Via:     &sip.Via{Host: publicIP},
		To:      &sip.Addr{Uri: requestURI},
		From:    &sip.Addr{Uri: &sip.URI{Host: publicIP, User: os.Getenv("USER")}},
		Contact: &sip.Addr{Uri: &sip.URI{Host: publicIP}},
		Payload: &sdp.SDP{
			Addr: publicIP,
			Origin: sdp.Origin{
				ID:   util.GenerateOriginID(),
				Addr: publicIP,
			},
			Audio: &sdp.Media{
				Port:   rtpPort,
				Codecs: []sdp.Codec{sdp.ULAWCodec, sdp.DTMFCodec},
			},
		},
		Warning: "Warning at SIP layer",
	}

	// Create SIP Dialog State Machine
	dl, err := dialog.NewDialog(invite)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create initialize SIP dialog: %s\n", err.Error())
		os.Exit(1)
	}

	// Send Audio Every 20ms
	var frame rtp.Frame
	awgn := dsp.NewAWGN(-45.0)
	ticker := time.NewTicker(ptime * time.Millisecond)
	defer ticker.Stop()

	// Ctrl+C or Kill Graceful Shutdown
	death := make(chan os.Signal, 1)
	signal.Notify(death, os.Interrupt, os.Kill)

	// DTMF Terminal Input
	keyboard := make(chan byte)
	keyboardStart := func() {
		go func() {
			var buf [1]byte
			for {
				amt, err := os.Stdin.Read(buf[:])
				if err != nil || amt != 1 {
					log.Printf("Keyboard: %s\r\n", err)
					return
				}
				keyboard <- buf[0]
			}
		}()
	}

	// Let's GO!
	var answered bool
	//var paerr C.int
	for {
		select {

		// Send Audio
		case <-ticker.C:
			for n := 0; n < psamps; n++ {
				frame[n] = awgn.Get()
			}

			if err := rs.Send(&frame); err != nil {
				log.Printf("RTP: %s\r\n", err.Error())
			}

		// Send DTMF
		case ch := <-keyboard:
			if err := rs.SendDTMF(ch); err != nil {
				log.Printf("DTMF: %s\r\n", err.Error())
				break
			}
			log.Printf("DTMF: %c\r\n", ch)

		// Receive Audio
		case frame := <-rs.C:
			if len(frame) != psamps {
				log.Printf("RTP: Received undersized frame: %d != %d\r\n", len(frame), psamps)
			} else {
				// Receiving audio - do nothing for now
			}
			rs.R <- frame

		// Signalling
		case rs.Peer = <-dl.OnPeer:
		case state := <-dl.OnState:
			switch state {
			case dialog.Answered:
				answered = true
				// notify to main logic that call was answered
				newEvent := CallEvent{ CALL_EVENT_ANSWER, "{}" }
				log.Printf("** New call event **: %d, %s\n", newEvent.EventType, newEvent.EventData)
				logicCh <- newEvent
				keyboardStart()
			case dialog.Hangup:
				if answered {
					return
				} else {
					os.Exit(1)
				}
			}

		// Logic instructions to modify the call management on the fly
	        case logicEvent :=  <-logicCh:
		        log.Printf("** New logic event **: %d, %s\n", logicEvent.EventType, logicEvent.EventData)
			// modify the call behaviour according to the event received

		// Errors and Interruptions
		case err := <-dl.OnErr:
			log.Fatalf("SIP: %s\r\n", err.Error())
		case err := <-rs.E:
			log.Printf("RTP: %s\r\n", err.Error())
			rs.CloseAfterError()
			dl.Hangup <- true
		case <-death:
			dl.Hangup <- true
		}
	}
}

func getPublicIP() (string, error) {
	resp, err := http.Get("http://api.ipify.org")
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}
